﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Entity;
using System.Reflection;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.Extensions.PlatformAbstractions;


namespace DataKethireddy.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            context.StudentRecords.RemoveRange(context.StudentRecords);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedStudentRecordsFromCsv(relPath, context);
        }


            //if (context.Locations.Any())
            //{
            //    return;   // DB already seeded
            //}
            //if (context.StudentRecords.Any())
            //{
            //    return;
            //}

            //var l1 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            //var l2 = new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };
            //var l3 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            //var l4 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
            //var l5 = new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
            //var l6 = new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };

            //context.AddRange(l1,l2,l3,l4,l5,l6);

            //context.StudentRecords.AddRange(
            //    new StudentRecord() {  studentSnumber = "s522501", lastName = "Pasupuleti", firstName = "Ramesh", email = "s522501@mail.nwmissouri.edu", phone = 6605281593, dateofbirth = 05/23/90, LocationID = l1.LocationID},
            //    new StudentRecord() {  studentSnumber = "s522521", lastName = "Thummala", firstName = "Rohan", email = "s522521@mail.nwmissouri.edu", phone = 6605281894, dateofbirth = 02/08/91, LocationID = l2.LocationID },
            //    new StudentRecord() {  studentSnumber = "s522592", lastName = "Kethireddy", firstName = "Swagath", email = "s522592@mail.nwmissouri.edu", phone = 9704729568, dateofbirth = 08/10/92, LocationID = l3.LocationID },
            //    new StudentRecord() {studentSnumber = "s522551", lastName = "Mohammod", firstName = "sardar", email = "s522551@mail.nwmissouri.edu", phone = 6605681693, dateofbirth = 04/06/90, LocationID = l4.LocationID },
            //    new StudentRecord() {  studentSnumber = "s522542", lastName = "Kethireddy", firstName = "Bhanusatvik", email = "s522592@mail.nwmissouri.edu", phone = 6605281736, dateofbirth = 08/10/ 92, LocationID = l5.LocationID },
            //    new StudentRecord() { studentSnumber = "s522561", lastName = "Rudraraju", firstName = "sandeep", email = "s522541@mail.nwmissouri.edu", phone = 6605683493, dateofbirth = 04/06/90, LocationID = l6.LocationID }
            //    );
            //context.SaveChanges();

            private static void SeedStudentRecordsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "studentrecord.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            StudentRecord.ReadAllFromCSV(source);
           
            List<StudentRecord> lst = StudentRecord.ReadAllFromCSV(source);
            context.StudentRecords.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }

    }
    }

