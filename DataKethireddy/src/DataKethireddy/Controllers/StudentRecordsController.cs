using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataKethireddy.Models;

namespace DataKethireddy.Controllers
{
    public class StudentRecordsController : Controller
    {
        private AppDbContext _context;

        public StudentRecordsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: StudentRecords
        public IActionResult Index()
        {
            //var appDbContext = _context.StudentRecords.Include(s => s.Location);
            return View(_context.StudentRecords.ToList());
        }

        // GET: StudentRecords/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            StudentRecord studentRecord = _context.StudentRecords.Single(m => m.studentID == id);
            if (studentRecord == null)
            {
                return HttpNotFound();
            }

            return View(studentRecord);
        }

        // GET: StudentRecords/Create
        public IActionResult Create()
        {
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location");
            return View();
        }

        // POST: StudentRecords/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(StudentRecord studentRecord)
        {
            if (ModelState.IsValid)
            {
                _context.StudentRecords.Add(studentRecord);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", studentRecord.LocationID);
            return View(studentRecord);
        }

        // GET: StudentRecords/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            StudentRecord studentRecord = _context.StudentRecords.Single(m => m.studentID == id);
            if (studentRecord == null)
            {
                return HttpNotFound();
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", studentRecord.LocationID);
            return View(studentRecord);
        }

        // POST: StudentRecords/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(StudentRecord studentRecord)
        {
            if (ModelState.IsValid)
            {
                _context.Update(studentRecord);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", studentRecord.LocationID);
            return View(studentRecord);
        }

        // GET: StudentRecords/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            StudentRecord studentRecord = _context.StudentRecords.Single(m => m.studentID == id);
            if (studentRecord == null)
            {
                return HttpNotFound();
            }

            return View(studentRecord);
        }

        // POST: StudentRecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            StudentRecord studentRecord = _context.StudentRecords.Single(m => m.studentID == id);
            _context.StudentRecords.Remove(studentRecord);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
